Source: khangman
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               dh-sequence-qmldeps,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.3.0~),
               gettext,
               libkeduvocdocument-dev (>= 4:24.08~),
               libkf6completion-dev (>= 6.3.0~),
               libkf6config-dev (>= 6.3.0~),
               libkf6coreaddons-dev (>= 6.3.0~),
               libkf6crash-dev (>= 6.3.0~),
               libkf6doctools-dev (>= 6.3.0~),
               libkf6i18n-dev (>= 6.3.0~),
               libkf6kio-dev (>= 6.3.0~),
               libkf6newstuff-dev (>= 6.3.0~),
               libkf6notifications-dev (>= 6.3.0~),
               qml6-module-org-kde-kirigamiaddons-components,
               qml6-module-org-kde-kirigamiaddons-delegates,
               qml6-module-org-kde-kirigamiaddons-formcard,
               qml6-module-qtmultimedia,
               qt6-base-dev (>= 6.5.0~),
               qt6-declarative-dev (>= 6.5.0~),
               qt6-declarative-private-dev,
               qt6-svg-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://edu.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/khangman
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/khangman.git

Package: khangman
Architecture: any
Section: misc
Depends: fonts-dustin,
         kdeedu-kvtml-data,
         ${misc:Depends},
         ${qml6:Depends},
         ${shlibs:Depends},
Suggests: khelpcenter,
Description: Hangman word puzzle
 KHangMan is the well-known Hangman game, aimed towards children aged 6 and
 above.
 .
 It picks a random word which the player must reveal by guessing if it contains
 certain letters.  As the player guesses letters, the word is gradually
 revealed, but 10 wrong guesses will end the game.
 .
 This package is part of the KDE education module.
